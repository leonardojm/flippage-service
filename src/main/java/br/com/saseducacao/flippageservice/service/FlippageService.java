package br.com.saseducacao.flippageservice.service;

import br.com.saseducacao.web.security.Grade;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlippageService {

    public Boolean isAuthorized(List<Grade> userGrades , Long gradeId, Long chapterId) {
        return userGrades.stream().filter(x -> gradeId.equals(x.getId())).findAny().isPresent();
    }
}