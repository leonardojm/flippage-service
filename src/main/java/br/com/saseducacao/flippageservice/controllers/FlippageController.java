package br.com.saseducacao.flippageservice.controllers;

import br.com.saseducacao.flippageservice.service.FlippageService;
import br.com.saseducacao.web.security.UserProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FlippageController {

    private final UserProvider userProvider;

    private final FlippageService flippageService;

    public FlippageController(UserProvider userProvider, FlippageService flippageService) {
        this.userProvider = userProvider;
        this.flippageService = flippageService;
    }

    @GetMapping("/grades/{gradeId}/chapters/{chapterId}/flippage")
    public ModelAndView getReponseAuthentication(@PathVariable(name = "gradeId") Long gradeId,
                                                 @PathVariable(name = "chapterId") Long chapterId) {

        ModelAndView mav = new ModelAndView();

        if (flippageService.isAuthorized(userProvider.user().getGrades(), gradeId, chapterId)) {
            mav.addObject("title", "Authorized");
            mav.addObject("body", chapterId);
            mav.setViewName("success");
            return mav;
        }
        mav.addObject("title", "401 - Unauthorized");
        mav.setViewName("error");
        return mav;
    }
}