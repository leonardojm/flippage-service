package br.com.saseducacao.flippageservice.client;

public class Chapter {
    private Long id;

    private Integer number;
    private String title;
    private String description;
    private Long bookId;

    public Chapter() {
    }

    public Chapter(Long id, Integer number, String title, String description, Long bookId) {
        this.id = id;
        this.number = number;
        this.title = title;
        this.description = description;
        this.bookId = bookId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }
}