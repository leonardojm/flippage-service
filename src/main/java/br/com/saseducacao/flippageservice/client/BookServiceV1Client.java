package br.com.saseducacao.flippageservice.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

import java.util.List;

@Headers("Accept: application/json")
public interface BookServiceV1Client {

    @RequestLine("GET /grades/{id}")
    Grade findGradesbyId(@Param("id") Long id);

    @RequestLine("GET /lectures/{id}")
    Lecture findLecturesById(@Param("id") Long id);

    @RequestLine("GET /chapters/{id}")
    Chapter findChapterById(@Param("id") Long id);

    @RequestLine("GET /grades/{gradeId}/lectures")
    List<Lecture> findLecturesByGradeId(@Param("id") Long gradeId);

    @RequestLine("GET /lectures/{lectureId}/chapters")
    List<Chapter> findChapterByLectureId(@Param("lectureId") Long lectureId);

}
