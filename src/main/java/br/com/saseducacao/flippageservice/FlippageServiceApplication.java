package br.com.saseducacao.flippageservice;

import br.com.saseducacao.web.security.config.WebSecurityConfig;
import br.com.saseducacao.web.security.config.WebSecurityConfigAdapter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({WebSecurityConfig.class, WebSecurityConfigAdapter.class})
public class FlippageServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlippageServiceApplication.class, args);
    }
}
